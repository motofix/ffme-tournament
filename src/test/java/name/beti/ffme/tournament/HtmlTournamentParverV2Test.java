package name.beti.ffme.tournament;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class HtmlTournamentParverV2Test {

  @Test
  @DisplayName("Parsing sample 01")
  void testSample01() throws Exception {
    TournamentQuery query = new TournamentQuery("sample", "sample");
    try (BufferedReader reader =
        new BufferedReader(
            new InputStreamReader(
                HtmlTournamentParverV2Test.class.getResourceAsStream("/query-sample-01.html")))) {
      List<Tournament> tournaments =
          new HtmlTournamentParverV2()
              .parse(query, reader.lines().collect(Collectors.joining()));
      assertThat(tournaments).hasSize(15);
    }
  }
}
