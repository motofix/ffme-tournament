package name.beti.ffme.tournament;

/**
 * @author Julien Béti - julien.beti@cosium.com
 */
public class AppException extends RuntimeException {
  public AppException(String message) {
    super(message);
  }

  public AppException(String message, Throwable cause) {
    super(message, cause);
  }
}
