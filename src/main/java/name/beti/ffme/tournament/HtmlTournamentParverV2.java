package name.beti.ffme.tournament;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class HtmlTournamentParverV2 {
  private static final Pattern P_TOURNAMENT_URL =
      Pattern.compile(TournamentParser.BASE_URL + "/competition/fiche/(\\d+)/");
  private static final Pattern P_STR_DATE = Pattern.compile("(\\d{1,2}) - (\\d{4})(.+)");
  private static final DateTimeFormatter DATE_FORMATTER =
      DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.FRENCH);

  public List<Tournament> parse(TournamentQuery query, String body) throws MalformedURLException {
    if (StringUtils.length(body) < 2) {
      return Collections.emptyList();
    }
/*    body = body.substring(1, body.length() - 1)
        .replace("\\\\\"", "\"")
        .replace("\\\\/", "/");
 */
    Document document = Jsoup.parse(body);
    Elements rows = document.select("div.competition");
    List<Tournament> tournaments = new ArrayList<>();
    for (Element row : rows) {
      this.parseRow(query, row).ifPresent(tournaments::add);
    }
    return tournaments;
  }

  private Optional<Tournament> parseRow(TournamentQuery tournamentQuery, Element row)
      throws MalformedURLException {
    Element link = row.select("h2.title a").first();
    if (link == null) {
      return Optional.empty();
    }
    String relativeTournamentUrl = link.attr("href");
    Matcher linkMatcher = P_TOURNAMENT_URL.matcher(relativeTournamentUrl);
    if (!linkMatcher.find()) {
      return Optional.empty();
    }
    Tournament tournament = new Tournament();
    tournament.setTournamentQuery(tournamentQuery);
    tournament.setTitle(link.text());
    tournament.setUrl(new URL(relativeTournamentUrl));
    tournament.setId(Long.parseLong(linkMatcher.group(1)));
    Element dateString = row.selectFirst("span.date");
    if (dateString != null) {
      tournament.setDate(this.parseDate(dateString));
    } else {
      return Optional.empty();
    }
    return Optional.of(tournament);
  }

  private LocalDate parseDate(Element htmlDate) {
    Matcher matcher = P_STR_DATE.matcher(htmlDate.text());
    if (!matcher.matches()) {
      return null;
    }
    return LocalDate.parse(
        matcher.group(1) + " " + matcher.group(3) + " " + matcher.group(2), DATE_FORMATTER);
  }
}
