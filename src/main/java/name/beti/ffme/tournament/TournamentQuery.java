package name.beti.ffme.tournament;

import java.io.Serializable;

/**
 * @author Julien Béti - julien@beti.name
 */
public class TournamentQuery implements Serializable {
  private final String name;
  private final String relativeUrl;

  public TournamentQuery(String name, String relativeUrl) {
    this.name = name;
    this.relativeUrl = relativeUrl;
  }

  String getName() {
    return name;
  }

  String getRelativeUrl() {
    return relativeUrl;
  }

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof TournamentQuery && ((TournamentQuery) obj).name.equals(this.name);
  }

  @Override
  public String toString() {
    return this.name;
  }
}
