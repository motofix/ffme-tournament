package name.beti.ffme.tournament;

/**
 * @author Julien Béti - julien@beti.name
 */
public class FfmeRemoteException extends RuntimeException {

  private final String url;

  public FfmeRemoteException(String url, Exception cause) {
    super(url, cause);
    this.url = url;
  }

  public String getUrl() {
    return url;
  }
}
