package name.beti.ffme.tournament;

import org.apache.commons.collections4.ListUtils;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class App {

  private static final String FFME_TOURNAMENTS_LIST_STORE_FILENAME = "ffme-tournaments-list.bin";

  public static void main(String[] args) {
    final Thread mainThread = Thread.currentThread();
    Runtime.getRuntime().addShutdownHook(new Thread(mainThread::interrupt));
    TournamentParser parser = new TournamentParser();
    List<Tournament> previousTournamentsFound = loadTournaments(parser);
    List<Notifier> notifiers = new ArrayList<>();
    notifiers.addAll(Configuration.getInstance().getFreeMobileNotifiers());
    notifiers.addAll(Configuration.getInstance().getSimpleMailNotifiers());
    System.out.printf("\nInitial tournaments loaded: %s", previousTournamentsFound.size());
    while (!Thread.currentThread().isInterrupted()) {
      try {
        Thread.sleep(3600000 * 2); // Every 2 hours
      } catch (InterruptedException e) {
        System.exit(0);
        Thread.currentThread().interrupt();
      }
      try {
        List<Tournament> tournamentsFound = parser.parse();
        System.out.printf("\nFound:");
        tournamentsFound.forEach(tournament -> System.out.printf("\n- %s", tournament));
        List<Tournament> delta = ListUtils.subtract(tournamentsFound, previousTournamentsFound);
        System.out.printf("\nDelta:");
        delta.forEach(tournament -> System.out.printf("\n- %s", tournament));
        previousTournamentsFound = tournamentsFound;
        if (delta.isEmpty()) {
          continue;
        }
        notifiers.forEach(Notifier::init);
        delta.forEach(
            tournament -> notifiers.forEach(notifier -> notifier.addTournament(tournament)));
        notifiers.forEach(Notifier::sendMessage);
        storeTournaments(previousTournamentsFound);
      } catch (AppException e) {
        System.out.printf("Error %s", e.getMessage());
        e.printStackTrace();
      }
    }
  }

  private static void storeTournaments(List<Tournament> tournaments) {
    Path store = Paths.get(FFME_TOURNAMENTS_LIST_STORE_FILENAME);
    try (OutputStream out =
        Files.newOutputStream(
            store, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
         ObjectOutputStream objectOut = new ObjectOutputStream(out)) {
      objectOut.writeObject(tournaments);
    } catch (IOException e) {
      System.err.println("Error storing tournaments from file: " + e.getMessage());
      e.printStackTrace();
    }
  }

  private static List<Tournament> loadTournaments(TournamentParser parser) {
    Path store = Paths.get(FFME_TOURNAMENTS_LIST_STORE_FILENAME);
    if (Files.notExists(store)) {
      System.err.println("No storage file found. Loading from parser");
      return parser.parse();
    }
    try (InputStream in = Files.newInputStream(store);
         ObjectInputStream objectIn = new ObjectInputStream(in)) {
      return (List<Tournament>) objectIn.readObject();
    } catch (IOException | ClassNotFoundException e) {
      System.err.println("Error loading tournaments from file: " + e.getMessage());
      e.printStackTrace();
      System.err.println("Loading from parser");
      return parser.parse();
    }
  }
}
