package name.beti.ffme.tournament;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/** @author Julien Béti - julien@beti.name */
class Configuration {
  private static final Configuration INSTANCE = new Configuration();
  private static final String NOTIFIER_FREE_MOBILE = "notifier.free-mobile";
  private final org.apache.commons.configuration2.Configuration backend;

  private Configuration() {
    FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
        new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
            .configure(
                new Parameters()
                    .properties()
                    .setFileName("/etc/ffme-tournament/configuration.properties")
                    .setThrowExceptionOnMissing(true)
                    .setListDelimiterHandler(new DefaultListDelimiterHandler(','))
                    .setIncludesAllowed(false));
    try {
      this.backend = builder.getConfiguration();
    } catch (ConfigurationException e) {
      throw new AppException("Unable to load local configuration file", e);
    }
  }

  static Configuration getInstance() {
    return INSTANCE;
  }

  SimpleEmail createSimpleEmail() {
    SimpleEmail email = new SimpleEmail();
    email.setHostName(this.backend.getString("email.host-name"));
    try {
      email.setFrom(this.backend.getString("email.from"));
    } catch (EmailException e) {
      throw new AppException("Error creating simple mail", e);
    }
    email.setSmtpPort(this.backend.getInt("email.smtp-port"));
    return email;
  }

  List<TournamentQuery> getQueries() {
    return this.getList(String.class, "queries").stream()
        .map(
            queryName ->
                new TournamentQuery(
                    queryName,
                    this.backend.getString("queries." + queryName + ".relative-url")))
        .collect(Collectors.toList());
  }

  List<FreeMobileSmsByHttp> getFreeMobileNotifiers() {
    return this.getList(String.class, NOTIFIER_FREE_MOBILE).stream()
        .map(
            userId ->
                new FreeMobileSmsByHttp(
                    this.getList(String.class,NOTIFIER_FREE_MOBILE + "." + userId + ".queries-names"),
                    this.backend.getString(NOTIFIER_FREE_MOBILE + "." + userId + ".user"),
                    this.backend.getString(NOTIFIER_FREE_MOBILE + "." + userId + ".password")))
        .collect(Collectors.toList());
  }

  List<SimpleEmailNotifier> getSimpleMailNotifiers() {
    return this.getList(String.class, "notifier.simple-mail").stream()
        .map(
            userId ->
                new SimpleEmailNotifier(
                    this.getList(String.class,"notifier.simple-mail." + userId + ".queries-names"),
                    this.backend.getString("notifier.simple-mail." + userId + ".email")))
        .collect(Collectors.toList());
  }

  private <U> List<U> getList(Class<U> clazz, String key) {
    List<U> list = this.backend.getList(clazz, key);
    if(list == null) {
      return Collections.emptyList();
    }
    return list;
  }
}
