package name.beti.ffme.tournament;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Julien Béti - julien.beti@cosium.com
 */
class FreeMobileSmsByHttp extends AbstractSimpleMessageNotifier {
  private static final String FREE_API_URL = "https://smsapi.free-mobile.fr/sendmsg";
  private final String user;
  private final String password;
  private final OkHttpClient client = new OkHttpClient();

  FreeMobileSmsByHttp(Collection<String> queryNames, String user, String password) {
    super(queryNames);
    this.user = user;
    this.password = password;
  }

  @Override
  public void sendSimpleMessage(String message) {
    HttpUrl httpUrl = HttpUrl.get(FREE_API_URL).newBuilder()
        .addQueryParameter("user", this.user)
        .addQueryParameter("pass", this.password)
        .addQueryParameter("msg", message).build();
    Request request = new Request.Builder().url(httpUrl).build();
    try(Response response = this.client.newCall(request).execute()) {
      if(!response.isSuccessful()) {
        throw new AppException("Error sending notification to " + this.user);
      }
    } catch (IOException e) {
      throw new AppException("Error sending notification to " + this.user, e);
    }
  }
}
