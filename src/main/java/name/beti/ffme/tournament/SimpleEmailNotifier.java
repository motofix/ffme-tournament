package name.beti.ffme.tournament;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;

import java.util.Collection;

/**
 * @author Julien Béti - julien@beti.name
 */
public class SimpleEmailNotifier extends AbstractSimpleMessageNotifier {
  private final String emailAddress;

  public SimpleEmailNotifier(Collection<String> queryNames, String emailAddress) {
    super(queryNames);
    this.emailAddress = emailAddress;
  }

  @Override
  public void sendSimpleMessage(String message) {
    Email email = Configuration.getInstance().createSimpleEmail();
    email.setSubject("Notification FFME: nouvelles compétitions trouvées");
    try {
      email.addTo(this.emailAddress);
      email.setMsg(message);
      email.send();
    } catch (EmailException e) {
      throw new AppException("Error sending notification email to " + email, e);
    }
  }
}
