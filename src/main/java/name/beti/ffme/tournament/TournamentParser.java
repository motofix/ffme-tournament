package name.beti.ffme.tournament;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** @author Julien Béti - julien@beti.name */
class TournamentParser {
  public static final String BASE_URL = "https://www.ffme.fr";
  private static final String SEARCH_PAGE_BASE_URL =
      BASE_URL + "/escalade/competition/calendrier-officiel-descalade/?posts_per_page=15";

  List<Tournament> parse() {
    return Configuration.getInstance().getQueries().stream()
        .flatMap(this::parse)
        .collect(Collectors.toList());
  }

  private Stream<Tournament> parse(TournamentQuery query) {
    String url = SEARCH_PAGE_BASE_URL + query.getRelativeUrl();
    System.out.printf("\nParsing %s: %s", query, url);
    int pageNumber = 0;
    List<Tournament> tournaments = new ArrayList<>();
    List<Tournament> currentPage;
    HtmlTournamentParverV2 htmlParser = new HtmlTournamentParverV2();
    try {
      do {
        // Temporising a little...
        Thread.sleep(Math.round(Math.random() * 10) * 1000);
        pageNumber++;
        System.out.printf("\nGetting page %s", pageNumber);
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response =
            httpClient.send(
                HttpRequest.newBuilder(new URI(url + "&pagination=" + pageNumber)).GET().build(),
                HttpResponse.BodyHandlers.ofString());
        currentPage = htmlParser.parse(query, response.body());
        tournaments.addAll(currentPage);
      } while (!currentPage.isEmpty() && pageNumber < 100);
      return tournaments.stream();
    } catch (URISyntaxException | IOException e) {
      throw new FfmeRemoteException(url, e);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      return Stream.empty();
    }
  }
}
