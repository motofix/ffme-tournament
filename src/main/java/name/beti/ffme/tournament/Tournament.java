package name.beti.ffme.tournament;

import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;

/**
 * @author Julien Béti - julien@beti.name
 */
class Tournament implements Serializable {
  private long id;
  private TournamentQuery tournamentQuery;
  private String title;
  private LocalDate date;
  private URL url;

  long getId() {
    return id;
  }

   void setId(long id) {
    this.id = id;
  }

  public TournamentQuery getTournamentQuery() {
    return tournamentQuery;
  }

  public void setTournamentQuery(TournamentQuery tournamentQuery) {
    this.tournamentQuery = tournamentQuery;
  }

  String getTitle() {
    return title;
  }

  void setTitle(String title) {
    this.title = title;
  }

  LocalDate getDate() {
    return date;
  }

  void setDate(LocalDate date) {
    this.date = date;
  }

  public URL getUrl() {
    return url;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  @Override
  public int hashCode() {
    return (int) (this.id % Integer.MAX_VALUE);
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof Tournament && ((Tournament) obj).id == id;
  }

  @Override
  public String toString() {
    return "Tournament{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", date=" + date +
        ", url=" + url +
        '}';
  }
}
