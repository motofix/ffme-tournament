package name.beti.ffme.tournament;

/**
 * Created on 14/01/2020.
 *
 * @author Julien Béti - julien@beti.name
 */
interface Notifier {
  void init();

  void addTournament(Tournament tournament);

  void sendMessage();
}
