package name.beti.ffme.tournament;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Julien Béti - julien@beti.name
 */
abstract class AbstractSimpleMessageNotifier implements Notifier {
  private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("dd/MM/yyyy");
  private final StringBuilder message = new StringBuilder();
  private final Set<String> queryNames = new HashSet<>();
  private boolean oneTournamentAccepted = false;

  AbstractSimpleMessageNotifier(Collection<String> queryNames) {
    this.queryNames.addAll(queryNames);
  }

  @Override
  public void init() {
    this.message.setLength(0);
    this.message.append("De nouvelles compétitions FFME ont été trouvées!");
  }

  @Override
  public void addTournament(Tournament tournament) {
    if(!this.queryNames.contains(tournament.getTournamentQuery().getName())) {
      return;
    }
    this.oneTournamentAccepted = true;
    this.message.append("\n- ")
        .append(tournament.getDate().format(DTF))
        .append(" - ")
        .append(tournament.getTitle())
        .append(" - ")
        .append(tournament.getUrl());
  }

  @Override
  public void sendMessage() {
    if(!oneTournamentAccepted) {
      return;
    }
    this.sendSimpleMessage(this.message.toString());
  }

  protected abstract void sendSimpleMessage(String message);
}
